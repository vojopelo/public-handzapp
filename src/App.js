import React, { Component } from 'react';
import './App.css';
import FacebookButton from './components/FacebookButton';
import FacebookLoginButton from './components/FacebookLoginButton';
import Album from './components/Album';
import Photos from './components/Photos';
import { connect } from 'react-redux';
import {updateLoginStatus,
        updateSelectedAlbum,
        updatePhotos,
        updateUser,
        updateAlbum} from './actions/app.actions';

class App extends Component {
    getAlbums(limitAlbums, failCallback) {
        window.FB.api('/me?fields=id,name,photos,albums.limit(' + limitAlbums + '){name,count,cover_photo{picture}}',
            response => {
                // Check if there is any error
                if(response && response.error) {
                    if(typeof failCallback === 'function') failCallback('error');
                    return;
                } else if(!response || !response.hasOwnProperty('albums')) {
                    if(typeof failCallback === 'function') failCallback('noResponse');
                    return;
                }

                //console.log(response);
                this.props.updateAlbum(response.albums.data);
                this.props.updateLoginStatus(true);
                this.props.updateUser(response.name);

		});
    }

    getPhotosInAlbum(albumId, limitPics, failCallback) {
         //Get index of album. Loose checking because id is string
         // Not removing the warning, so toString
		const index = this.props.albums.findIndex(album => album.id === albumId.toString());

		if(index === -1) {
			if(typeof failCallback === 'function') failCallback('noAlbum');
			return;
		}

		window.FB.api(albumId + '/?fields=photos.limit(' + limitPics + '){picture,images}', response => {
            // Check if there is any error
			if(response && response.error) {
				if(typeof failCallback === 'function') failCallback('error');
				return;
			} else if(!response || !response.hasOwnProperty('photos')) {
				if(typeof failCallback === 'function') failCallback('noResponse');
				return;
            }

            //console.log('------ Photos', response);

			response.photos.data.forEach(photo => {
                //[0] image
                photo.picture_full = photo.images[0].source; 

                //Only need one full image
				delete photo.images; 
            });

            //console.log('status ----- ', this.state.albums[index]);
            //console.log('----- response  ', response.photos.data)

            let selAlbum = this.props.albums[index];
            this.props.updateSelectedAlbum(selAlbum);
            this.props.updatePhotos(response.photos.data);
		});
    }

    showPhotos = (albumId) => {
        // console.log('albumId ', albumId)
        this.getPhotosInAlbum (albumId, 10, this.errorHandler);
    }

    errorHandler = response => {
        let userMsg;

        if(response === 'error') {
            userMsg = 'Error getting albums';
        } else if(response === 'noResponse') {
            userMsg = 'No albums';
        } else if(response === 'noAlbum') {
            userMsg = 'No albums';
        }

        console.log('response ', response)

        alert('', userMsg);
    }

    getLoginStatusWORK () {
        this.FB = window.FB;
        this.FB.getLoginStatus(this.handleFacebookLoginResponse);
    }

    onFacebookLogin = (loginStatus, resultObject) => {
        if (loginStatus === true) {
          this.setState({
            username: resultObject.user.name
          });
          this.getAlbums(15, this.errorHandler);
        } 
        // else {
        //   alert('Facebook login error');
        // }
      }

    handleFacebookLoginResponse = (res) => {
        // console.log('Inside handleFacebookLoginResponse')
        console.log('handleFacebookLoginResponse props', this.props);
        window.FB.login(this.props.onLogin, {scope: 'public_profile,user_photos'});
        console.log('handleFacebookLoginResponse ', res);
        if (res.status === 'connected') {
            this.getAlbums(15, this.errorHandler);
        }
    }

    componentDidMount () {
        //document.addEventListener('onFBLogIn', this.getLoginStatus)
    }

    componentWillUnmount () {
        //document.removeEventListener('onFBLogIn', this.getLoginStatus)
    }

    onFacebookLogout () {
        window.FB.logout(function(resp){
            // reload so that cookies get removed
            window.location.reload();
        })
    }

    render () {
        //console.log('inside render ', this.props)
        return (
            // <div className="App">
            // <br /><br />
            //         <div>
            //             <FacebookButton
            //                 onLogin={this.onFacebookLogin}
            //                 onLogout={this.Logout}>
            //             </FacebookButton>
            //         </div>

            //     {(this.props.user) &&
            //         <Album albums={this.props.albums} 
            //         showPhotos={this.showPhotos}/>
            //     }
            //     {(this.props.photos) &&
            //         <div style={{borderTop:'1px solid #000', padding: '50px'}}>
            //             <span>Photos for {this.props.selectedAlbum.name}</span>
            //             <Photos photos={this.props.photos} />
            //         </div>
            //     }
            // </div>

                <div className="App">
                <br /><br />
                { !this.props.user &&
                <div>
                    <p>Login to Facebook to see the albums and pictures inside them</p>
                    <FacebookLoginButton onLogin={this.onFacebookLogin}>
                    <button>Login to Facebook</button>
                    </FacebookLoginButton>
                </div>
                }
                {this.props.user &&
                <p>Welcome back, {this.props.user}
                <button onClick={this.onFacebookLogout}>Logout</button>
                </p>
                }

                {(this.props.user) &&
                    <Album albums={this.props.albums} 
                    showPhotos={this.showPhotos}/>
                }
                {(this.props.photos) &&
                    <div style={{borderTop:'1px solid #000', padding: '50px'}}>
                        <span>Photos for {this.props.selectedAlbum.name}</span>
                        <Photos photos={this.props.photos} />
                    </div>
                }
                </div>
        )
    }
}

const mapStateToProps = (state) => {
    //console.log('inside mapStateToProps', state)
    return ({
        user: state.user,
        albums: state.albums,
        photos: state.photos,
        selectedAlbum: state.selectedAlbum,
        isLoggedIn: state.isLoggedIn
    })
}

const mapDispatchToActions = (dispatch) => {
    return ({
        updateLoginStatus: (logInStatus) => { 
            dispatch(updateLoginStatus(logInStatus));
        },
        updateSelectedAlbum: (selAlbum) => {
            dispatch(updateSelectedAlbum(selAlbum));
        },
        updatePhotos: (photos) => {
            dispatch(updatePhotos(photos));
        },
        updateUser: (userInfo) => {
            dispatch(updateUser(userInfo));
        },
        updateAlbum: (album) => {
            dispatch(updateAlbum(album));
        }
    })
}

export default connect(mapStateToProps, mapDispatchToActions)(App);
