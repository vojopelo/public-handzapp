export const LOGIN_STATUS = 'LOGIN_STATUS';
export const UPDATE_SELECTED_ALBUM = 'UPDATE_SELECTED_ALBUM';
export const UPDATE_PHOTOS = 'UPDATE_PHOTOS';
export const UPDATE_USER_INFO = 'UPDATE_USER_INFO';
export const UPDATE_ALBUM = 'UPDATE_ALBUM';

export const updateLoginStatus = (logInStatus) => {
    return {type: LOGIN_STATUS, payload: logInStatus};
};

export const updateSelectedAlbum = (selAlbum) => {
    return {type: UPDATE_SELECTED_ALBUM, payload: selAlbum};
}

export const updatePhotos = (photos) => {
    return {type: UPDATE_PHOTOS, payload: photos};
}

export const updateUser = (userInfo) => {
    return {type: UPDATE_USER_INFO, payload: userInfo};
}

export const updateAlbum = (album) => {
    return {type: UPDATE_ALBUM, payload: album};
}