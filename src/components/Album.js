import React, { Component } from 'react'

export default class Album extends Component {

    showPhotos = (albumId) => {
        this.props.showPhotos(albumId);
    }

    showAlbumCoverImage = (albums) => {
        let imgs = [];
        for(let i =0; i < albums.length; i++) {
            let album = albums[i];
            
            // console.log(album);
            if (album.hasOwnProperty('cover_photo')) {
                imgs.push(
                    <React.Fragment key={album.id}>
                        <img src={album.cover_photo.picture} 
                            style={{margin: '10px',
                                    padding:'20px',
                                    border: '2px solid #000',
                                    borderRadius: '50px 15px'
                                    }}
                            alt={album.name}
                            onClick={() => this.showPhotos(album.id)}/>
                        <span>{album.name}</span>
                    </React.Fragment>);
            }
        }
        return imgs;
    }

    render() {
        return (
        <div>
            {this.showAlbumCoverImage(this.props.albums)}
        </div>
    )
  }
}
