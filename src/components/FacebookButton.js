import React, { Component } from 'react'

class FacebookButton extends Component {
    state = {
        isLoggedIn: false
    }

    componentDidMount () {
        document.addEventListener('onFBLogIn', this.getLoginStatus)
    }

    componentWillUnmount () {
        document.removeEventListener('onFBLogIn', this.getLoginStatus)
    }

    loginToFacebook = () => {
        if (!window.FB) return;

        // Check again if the user is logged on or cancelled
        window.FB.getLoginStatus(res => {
            if (res.status === 'connected') {
                this.setState ({
                    isLoggedIn: true
                })
                this.props.onLogin(res);
            } else {
                window.FB.login(this.facebookLoginHandler, {scope: 'public_profile,user_photos'});
            }
        },);
    }

    logout = () => {
        this.setState ({
            isLoggedIn: false
        })
        this.props.onLogout();
    }

    render () {
        // let {children} = this.props;
        // return (
        //     <div onClick={this.loginToFacebook}>{children}</div>
        // )
        return (
            <React.Fragment>
                {(!this.state.isLoggedIn) && 
                    <button onClick={this.loginToFacebook}>Login</button>
                }
                {(this.state.isLoggedIn) && 
                    <button onClick={this.logout}>Logout</button>
                }
            </React.Fragment>

            
        )
    }
}

export default FacebookButton;
