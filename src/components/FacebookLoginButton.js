import React, { Component } from 'react';

export default class FacebookLoginButton extends Component {
    componentDidMount () {
        document.addEventListener('onFBLogIn', this.initializeFacebookLogin)
    }

    componentWillUnmount () {
        // To avoid memory leaks
        document.removeEventListener('onFBLogIn', this.initializeFacebookLogin)
    }

    // Initialize FB and check the FB login status
    initializeFacebookLogin = () => {
        this.FB = window.FB;
        this.checkFBLoginStatus();
    }

    // Check the login status
    checkFBLoginStatus = () => {
        this.FB.getLoginStatus(this.handleFacebookLoginResponse);
    }

    // Handle FB response
    handleFacebookLoginResponse = (res) => {
        if (res.status === 'connected') {
            this.FB.api('/me', userData => {
                this.props.onLogin(true, {...res, user: userData});
            })
        }
    }

    fbLoginButtonClick = () => {
        if (!this.FB) this.FB = window.FB;

        this.FB.getLoginStatus(res => {
            if (res.status === 'connected') {
                this.handleFacebookLoginResponse(res);
            } else {
                this.FB.login(this.handleFacebookLoginResponse, {scope: 'public_profiles'});
            }
        },);
    }

    render () {
        let {children} = this.props;
        return (
            <div onClick={this.fbLoginButtonClick}>
                {children}
            </div>
        )
    }
}