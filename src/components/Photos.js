import React, { Component } from 'react'

export default class Photos extends Component {

    showImages = (albumId) => {
        this.props.showPictures(albumId);
    }

    showPhotos = (photos) => {
        let imgs = [];
        for(let i =0; i < photos.length; i++) {
            let photo = photos[i];
            // console.log('-------------Photo ', photo)
            imgs.push(
                <React.Fragment key={i}>
                    <img src={photo.picture} 
                        style={{margin: '10px',
                                padding:'20px',
                                border: '2px solid #000',
                                borderRadius: '50px 50px'
                                }}
                        alt={photo}/>
                </React.Fragment>);
        }
        return imgs;
    }

    render() {
        return (
        <div>
            {this.showPhotos(this.props.photos)}
        </div>
    )
  }
}
