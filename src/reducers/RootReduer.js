import {LOGIN_STATUS, 
    UPDATE_SELECTED_ALBUM, 
    UPDATE_PHOTOS, 
    UPDATE_USER_INFO, 
    UPDATE_ALBUM} from '../actions/app.actions'

const initState = {
    user: null,
    albums: null,
    photos: null,
    selectedAlbum: null,
    isLoggedIn: false
};

const rootReducer = (state = initState, action) => {
    console.log(action)

    if(!action) return state;

    switch(action.type) {
        case LOGIN_STATUS: 
            return {
                ...state,
                isLoggedIn: action.payload
            };

        case UPDATE_SELECTED_ALBUM:
            return {
                ...state,
                selectedAlbum: action.payload
            };

        case UPDATE_PHOTOS: 
            return {
                ...state,
                photos: action.payload
            };

        case UPDATE_USER_INFO: 
            return {
                ...state,
                user: action.payload
            };

        case UPDATE_ALBUM: 
            console.log('before updating ', action.payload)
            return {
                ...state,
                albums: action.payload
            };

        default: return state;
    }
}

export default rootReducer;